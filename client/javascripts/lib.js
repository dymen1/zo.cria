"use strict";

/**
 * Created by D on 22-5-14.
 */
(function () {
    window.crud = {

        bakeCookie: function (cname, cvalue, exdays) {
            var d, expires;
            d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            expires = "expires=" + d.toGMTString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        },

        getCookieFromJar: function (cname) {
            var name, i, ca, c;
            name = cname + "=";
            ca = document.cookie.split(';');
            for (i = 0; i < ca.length; i += 1) {
                c = ca[i].trim();
                if (c.indexOf(name) === 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        },

        eatCookie: function (cname) {
            document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
        },

        toggle_visibility: function (id) {
            var e = document.getElementById(id);
            if (e.style.display === 'block') {
                e.style.display = 'none';
            } else {
                e.style.display = 'block';
            }
        },

        /**
         * function to "show" an element and grey-out the button that showed it
         */
        showElement: function (classname, buttonNodeClass, multipleOfSameType) {
            var i,
                node,
                elementsWClassname = document.getElementsByClassName(classname),
                elementsWBtnClass = document.getElementsByClassName(buttonNodeClass),
                elementMouseIsOver = document.elementFromPoint(event.clientX, event.clientY),
                divElement = elementMouseIsOver.parentNode.parentNode.parentNode.childNodes;

            if (elementsWClassname === null) {
                console.log("No element with that classname exists");
            } else if (multipleOfSameType) {
                for (i = 0; i < divElement.length; i += 1) {
                    if (divElement[i].nodeName === "DIV") {
                        divElement[i].style.display = "block";
                    }
                }

                if (elementsWBtnClass !== undefined) {
                    node = elementMouseIsOver;
                    node.style.webkitFilter = 'grayscale(1)';
                    node.style.mozFilter = 'grayscale(1)';
                    node.style.oFilter = 'grayscale(1)';
                    node.style.msFilter = 'grayscale(1)';
                    node.style.filter = 'grayscale(1)';
                    node.style.cursor = 'default';
                }
            } else if (elementsWClassname.length === 1) {
                elementsWClassname[0].style.display = "block";

                if (elementsWBtnClass !== undefined && elementsWBtnClass.length !== 0) {
                    node = elementsWBtnClass[0];
                    node.style.webkitFilter = 'grayscale(1)';
                    node.style.mozFilter = 'grayscale(1)';
                    node.style.oFilter = 'grayscale(1)';
                    node.style.msFilter = 'grayscale(1)';
                    node.style.filter = 'grayscale(1)';
                    node.style.cursor = 'default';
                }
            }
        },

        /**
         * function to "hides" an element and un-greys-out the button that showed it
         */
        hideElement: function (classname, buttonNodeClass, multipleOfSameType) {
            var i,
                node,
                elementsWClassname = document.getElementsByClassName(classname),
                elementsWBtnClass = document.getElementsByClassName(buttonNodeClass),
                elementMouseIsOver = document.elementFromPoint(event.clientX, event.clientY),
                divElement = elementMouseIsOver.parentNode.parentNode.parentNode.childNodes;

            if (elementsWClassname === null) {
                console.log("No element with that classname exists");
            } else if (multipleOfSameType) {
                for (i = 0; i < divElement.length; i += 1) {
                    if (divElement[i].nodeName === "DIV") {
                        divElement[i].style.display = "none";
                    }
                }

                if (elementsWBtnClass !== undefined) {
                    node = elementMouseIsOver;
                    node.style.webkitFilter = 'none';
                    node.style.mozFilter = 'none';
                    node.style.oFilter = 'none';
                    node.style.msFilter = 'none';
                    node.style.filter = 'none';
                    node.style.cursor = 'hand;';
                }
            } else if (elementsWClassname.length === 1) {
                elementsWClassname[0].style.display = "none";

                if (elementsWBtnClass !== undefined && elementsWBtnClass.length !== 0) {
                    node = elementsWBtnClass[0];
                    node.style.webkitFilter = 'none';
                    node.style.mozFilter = 'none';
                    node.style.oFilter = 'none';
                    node.style.msFilter = 'none';
                    node.style.filter = 'none';
                    node.style.cursor = 'hand';
                }
            }
        },

        /**
         * function that "hides" all the elements on a page with class overlay
         */
        removeOverlay: function () {
            var i, overlayNodeList = document.getElementsByClassName("overlay");
            if (overlayNodeList[0] !== "undefined") {
                for (i = 0; i < overlayNodeList.length; i += 1) {
                    if (overlayNodeList[i].style.display !== "none") {
                        overlayNodeList[i].style.display = "none";
                    }
                }
            }
        },

        /**
         * function that "shows" all the elements on a page with class overlay
         */
        displayOverlay: function () {
            var i, overlayNodeList = document.getElementsByClassName("overlay");
            if (overlayNodeList[0] !== "undefined") {
                for (i = 0; i < overlayNodeList.length; i += 1) {
                    if (overlayNodeList[i].style.display !== "block") {
                        overlayNodeList[i].style.display = "block";
                    }
                }
            }
        },

        //sets answer styling depending on the current number of answers
        setAnswerColumnWidth: function () {
            var aContList = document.getElementsByClassName("answerContainer");
            console.log("aContList", aContList.length);
        },

        /**
         * menu button code
         * TODO clean up this code
         */
        updateNodes: function () {
            var floatInX = -70,
                floatOutX = 90,
                elementMouseIsOver = document.elementFromPoint(event.clientX, event.clientY),
                menu = document.getElementById("headerMenuButton"),
                menuContent = document.getElementById("nonLoggedMenu"),
                menuContentLogged = document.getElementById("loggedMenu"),
                menuElement;
            if (menuContent.style.display === "block") {
                menuElement = menuContent;
            } else if (menuContentLogged.style.display === "block") {
                menuElement = menuContentLogged;
            }
            menuElement.childNodes[1].style.height = screen.height + "px";
            menuElement.childNodes[1].style.height = screen.height + "px";

            //is inverted because the JQuery from bootstrap is run before this JS
            if (menu.parentNode.classList.contains("open") === false) {
                if (menuElement.childNodes[1].style.left !== floatOutX + "px") {
                    menuElement.childNodes[1].style.left = floatOutX + "px";
                }
            } else if (elementMouseIsOver === menu || elementMouseIsOver.parentNode === menu) {
                if (menuElement.childNodes[1].style.left !== floatInX + "px") {
                    menuElement.childNodes[1].style.left = floatInX + "px";
                }
            }
        }
    };
}());

/**
 * On-load function that calls the init.
 */
window.onload = function () {
//    window.crud.setBGimg();
};

/**
 * Onclick and space-bar event functions.
 */
document.onclick = function (e) {
    var crud = window.crud;
    if (1 === e.which || 1 === e.button) {
        crud.updateNodes();
    }
};