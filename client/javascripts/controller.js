/*global angular */
"use strict";

/*HOME*/
function HomeCtrl($scope) {
    //sets the background image
    //TODO check if this can be done via Less (css programming language)
    document.body.style.backgroundImage = "url('../images/achtergrond-homepage.png')";
    document.body.style.backgroundPosition = "0% 100%";
    //sets footer
    //TODO check if this can be done via Less (css programming language)
    document.getElementsByTagName("footer")[0].style.display = "none";

    $scope.$on('updateHomepage', function () {
        $scope.updateHomepage();
    });

    $scope.updateHomepage = function () {
        if (window.crud.getCookieFromJar("loggedInUsername") !== "") {
            var i, linkNodes = document.getElementsByClassName("link");
            for (i = 0; i < linkNodes.length; i += 1) {
                linkNodes[i].href = "/#/templates";
            }
        }
    };

    //update homepage links
    $scope.updateHomepage();
}

/*TEMPLATES*/
function TemplateCtrl() {
    document.body.style.backgroundImage = "none";//url('../images/overons.jpg')";
    document.body.style.backgroundPosition = "0% 0%";
    document.getElementsByTagName("footer")[0].style.display = "block";

    if (window.crud.getCookieFromJar("newAccountMessage") === "1") {
        document.getElementById("newAccountMessage").style.display = "block";
        window.crud.bakeCookie("newAccountMessage", 0, 1);
    }
}

/*ABOUTUS*/
function AboutUsCtrl() {
    document.body.style.backgroundImage = "none";//url('../images/overons.jpg')";
    document.body.style.backgroundPosition = "0% 0%";
    document.getElementsByTagName("footer")[0].style.display = "block";
}

/*HOEWERKTHET*/
function HoeWerktHetCtrl() {
    document.body.style.backgroundImage = "none";//url('../images/overons.jpg')";
    document.body.style.backgroundPosition = "0% 0%";
    document.getElementsByTagName("footer")[0].style.display = "block";
}

/*CONTACT*/
function ContactCtrl() {
    document.body.style.backgroundImage = "none";
    document.getElementsByTagName("footer")[0].style.display = "block";
}

/*USER*/
function UserListCtrl($scope, userService) {
    document.body.style.backgroundImage = "none";
    document.getElementsByTagName("footer")[0].style.display = "block";
    $scope.user = userService.user.get();
}


function UserDetailCtrl($scope, $window, $routeParams, userService, $location, passingService) {
    document.body.style.backgroundImage = "none";
    document.getElementsByTagName("footer")[0].style.display = "block";
    if (window.crud.getCookieFromJar("loggedIn_ID") === String(0)) {
        var containerNode, newNode = document.createElement("h1");
        newNode.textContent = "U bent admin";
        containerNode = document.getElementById("contentContainer");
        while (containerNode.firstChild) {
            containerNode.removeChild(containerNode.firstChild);
        }
        containerNode.appendChild(newNode);
    }
    $scope.user = userService.user.get({_id: $routeParams._id}, function () {
//        console.log('$scope.user ', $scope.user);
    });
    // remove user
    $scope.remove = function () {
        console.log('entering remove');
        userService.user.remove({_id: $routeParams._id});
        $location.path("/user/");
    };

    $scope.loginRegister = function (username, password) {
        console.log("login");
        var messageObject = {
            "msgUsername": username,
            "msgPassword": password
        };
        passingService.prepForBroadcast('login', messageObject);
    };

    // CREATE, UPDATE user
    $scope.save = function () {
        if ($scope.user.doc && $scope.user.doc._id !== undefined) {
            console.log('entering update');
            console.log($scope.user);
            $scope.user.doc.groups = $scope.user.groups;

            userService.user.update({_id: $scope.user.doc._id}, $scope.user.doc, function (res) {
                $window.location.href = "/#/user/" + $scope.user.doc._id;
                console.log($scope.user);
            });
        } else {
            console.log('entering save');
            console.log('new acc', $scope.user.doc);
            userService.user.save({}, $scope.user.doc, function (res) {
                window.crud.bakeCookie("newAccountMessage", 1, 1);
                $scope.loginRegister($scope.user.doc.username, $scope.user.doc.password);
                console.log("res", res);
            });
        }
    };
}

/*Groups*/
function GroupListCtrl($scope, groupService) {
    document.body.style.backgroundImage = "none";
    document.getElementsByTagName("footer")[0].style.display = "block";
    $scope.group = groupService.group.get();
}

function GroupDetailCtrl($scope, $routeParams, groupService, $location) {
    document.body.style.backgroundImage = "none";
    document.getElementsByTagName("footer")[0].style.display = "block";
    console.log("test", $routeParams._id);
    $scope.group = groupService.group.get({_id: $routeParams._id}, function () {
        console.log('$scope.group ', $scope.group);
    });
    // remove group
    $scope.remove = function () {
        console.log('entering remove');
        groupService.group.remove({_id: $routeParams._id});
        $location.path("/group/");
    };
    // CREATE, UPDATE group
    $scope.save = function () {
        if ($scope.group.doc && $scope.group.doc._id !== undefined) {
            console.log('entering update');
            groupService.group.update({_id: $scope.group.doc._id}, $scope.group.doc, function (res) {
                console.log("res", res);
            });
        } else {
            console.log('entering save');
            groupService.group.save({}, $scope.group.doc, function (res) {
                console.log("res", res);
            });
        }
    };
}

/*Index*/
function UserLoginCtrl($scope, $http, $window, passingService) {
    document.body.style.backgroundImage = "none";
    document.getElementsByTagName("footer")[0].style.display = "block";
    var isVerified, partial;
    /**
     * Initial file to get the menu partial
     * @returns {*}
     */
    $scope.getMenuPartial = function () {
        partial = "/partials/menu.html";
        $scope.src = partial;
        return partial;
    };

    $scope.setMenuPartial = function (logged) {
        var loggedMenu = document.getElementById("loggedMenu"), nonLoggedMenu = document.getElementById("nonLoggedMenu");
        if (logged) {
            if (loggedMenu !== null || nonLoggedMenu !== null) {
                loggedMenu.style.display = "block";
                nonLoggedMenu.style.display = "none";
            }
        } else {
            if (loggedMenu !== null || nonLoggedMenu !== null) {
                loggedMenu.style.display = "none";
                nonLoggedMenu.style.display = "block";
            }
        }
    };

    /**
     * Initial file to get the partial
     * @returns {*}
     */
    $scope.getPartial = function () {
        $scope.username = window.crud.getCookieFromJar("loggedInUsername");
        $scope._id = window.crud.getCookieFromJar("loggedIn_ID");

        if ($scope._id !== undefined) {
            $scope.fillAllScopes($scope._id);
        }

        if (isVerified === undefined) {
            $http({
                method: "GET",
                url: "/account"
            }).
                success(function (data) {
                    isVerified = data.isVerified;

                    if (isVerified === true) {
                        partial = "/partials/logout.html";
                        $scope.setMenuPartial(true);
                    } else {
                        partial = "/partials/login.html";
                        $scope.setMenuPartial(false);
                    }
                    $scope.src = partial;
                    return partial;
                });
        }

        if (isVerified === true) {
            partial = "/partials/logout.html";
            $scope.setMenuPartial(true);
        } else {
            partial = "/partials/login.html";
            $scope.setMenuPartial(false);
        }
        $scope.src = partial;
        return partial;
    };

    $scope.updateHomepage = function (doc) {
        passingService.prepForBroadcast('updateHomepage', doc);
    };

    $scope.fillAllScopes = function (doc) {
        passingService.prepForBroadcast('fillAllScopes', doc);
    };

    $scope.$on('fillAllScopes', function () {
        $scope._id = passingService.message;
    });

    $scope.$on('login', function () {
        var loginForm = {
            "username": passingService.message.msgUsername,
            "password": passingService.message.msgPassword
        };
        $scope.login(loginForm);
    });
    /**
     * Process login and, if successful login, get new partial and redirect.
     * @param loginForm
     */
    $scope.login = function (loginForm) {
        if (loginForm !== undefined && loginForm.username !== undefined && loginForm.password !== undefined) {
            $http({
                method: "POST",
                url: "/login",
                data: {"username": loginForm.username, "password": loginForm.password}
            }).
                success(function (data) {
                    console.log(data);
                    isVerified = data.isVerified;

                    window.crud.bakeCookie("loggedInUsername", loginForm.username, 1);
                    if (loginForm.username === "admin") {
                        window.crud.bakeCookie("loggedIn_ID", 0, 1);
                    } else {
                        window.crud.bakeCookie("loggedIn_ID", data.user[0]._id, 1);
                    }
                    $scope.username = window.crud.getCookieFromJar("loggedInUsername");
                    $scope._id = window.crud.getCookieFromJar("loggedIn_ID");
                    $scope.updateHomepage();

                    if (isVerified === true) {
                        // load partial
                        partial = "/partials/logout.html";
                        $scope.src = partial;
                        // redirect to admin area
                        console.log("heui", typeof window.crud.getCookieFromJar("newAccountMessage"), window.crud.getCookieFromJar("newAccountMessage"));
                        if (window.crud.getCookieFromJar("newAccountMessage") === "0" || window.crud.getCookieFromJar("newAccountMessage") === "") {
                            window.location = "#/";
                        } else {
                            window.location = "#/templates";
                        }
                    } else {
                        window.location = "#/";
                        $scope.msg = "No valid email or password.";
                    }
                }).
                error(function (data) {
                    console.log(data);
                    var i, loginFormNodeList = document.getElementsByClassName("loginFormInput");
                    for (i = 0; i < loginFormNodeList.length; i += 1) {
                        loginFormNodeList[i].style.border = "1px #a94442 solid";
                    }
                });
        } else {
            var i, loginFormNodeList = document.getElementsByClassName("loginFormInput");
            for (i = 0; i < loginFormNodeList.length; i += 1) {
                loginFormNodeList[i].style.border = "1px #a94442 solid";
            }
        }
    };

    /**
     * Process logout, then get new partial and redirect.
     */
    $scope.logout = function () {
        console.log('logout');

        $http({method: 'GET', url: '/logout'})
            .success(function (data, status, headers, config) {
                window.crud.eatCookie("loggedInUsername");
                window.crud.eatCookie("loggedIn_ID");

                // this callback will be called asynchronously
                // when the response is available
                partial = "/partials/login.html";
                $scope.src = partial;
                // redirect to admin area
                $window.location.href = "/";

            });
    };

}

function EnqueteListCtrl($scope, enqueteService, $location, mediaService) {
    document.body.style.backgroundImage = "none";
    document.getElementsByTagName("footer")[0].style.display = "block";

    $scope.getEnqueteLogo = function (enqueteIndex, logoNodeList) {
        console.log($scope.enquete.doc[enqueteIndex].logo);
        mediaService.media.get({_id: $scope.enquete.doc[enqueteIndex].logo}, function (res) {
            logoNodeList[enqueteIndex].src = res.doc.path;
        });
    };
    enqueteService.enquete.get(function (res) {
        $scope.enquete = res;
        var enqueteIndex, logoNodeList = document.getElementsByClassName("overviewLogo");
        console.log("get enquete res", res);
        for (enqueteIndex = 0; enqueteIndex < res.doc.length; enqueteIndex += 1) {
            $scope.getEnqueteLogo(enqueteIndex, logoNodeList);
        }
    });

    var partial;
    /**
     * Initial file to get the partial
     * @returns {*}
     */
    $scope.getPartial = function () {
        partial = "/partials/enquete-new.html";
        $scope.src = partial;
        return partial;
    };

    // CREATE enquete (deprecated)
    $scope.save = function () {
        $scope.enquete.doc = {
            poll: $scope.enquete.doc.poll,
            isPublished: $scope.enquete.doc.isPublished
        };
        console.log('entering save');
        enqueteService.enquete.save({}, $scope.enquete.doc, function (res) {
            $location.path("/enquete/" + res.doc._id);
            console.log("res enquete", res);
        });
    };
}

function EnqueteDetailCtrl($scope, $routeParams, enqueteService, passingService, $location, mediaService) {
    //array used to make sure that all questions in a enquete are unique
    $scope.tempData = [];
    //sets background image
    document.body.style.backgroundImage = "none";
    //sets footer
    document.getElementsByTagName("footer")[0].style.display = "block";
    $scope.enquete = enqueteService.enquete.get({_id: $routeParams._id}, function () {
        console.log($scope);
        if ($scope.enquete.doc.logo !== "") {
            mediaService.media.get({_id: $scope.enquete.doc.logo}, function (res) {
                console.log("enquête logo", res);
                // in the enquete overview none of this is shown
                if (!(window.location.href.indexOf("overview") > -1)) {
                    document.getElementsByClassName("enqueteMedia")[0].src = res.doc.path;
                }
            });
        }
        // in the enquete overview none of this is shown
        if (!(window.location.href.indexOf("overview") > -1)) {
            var i;
            for (i = 0; i < $scope.enquete.questions.length; i += 1) {
                console.log($scope.enquete.questions[i].media);
                $scope.getMedia(i, $scope.enquete.questions[i]);
            }
        }
    });

    // attempt to influence the styling of answers according to the amount of answer linked to a question,
    // unfortunately this method didn't work because of the timing of the document ready events.
    // (there are still request in progress when it fires)
    //TODO find a way to make to make this work
    $scope.$on('$viewContentLoaded', function () {
//        console.log("ready");
//        window.crud.setAnswerColumnWidth();
    });

    //gets the media for a question if that question has any media linked to it
    $scope.getMedia = function (i, question) {
        if (question.media !== "") {
            mediaService.media.get({_id: question.media}, function (res) {
                console.log("question media", res);
                document.getElementsByClassName("questionMedia")[i].src = res.doc.path;
            });
        }
    };

    //used in saving a enquete
    $scope.check = function (answer) {
        var sameQuestion, i;
        for (i = 0; i < $scope.tempData.length; i += 1) {
            if (answer.questionID === $scope.tempData[i].questionID) {
                $scope.tempData[i] = {answer: answer};
                sameQuestion = true;
            }
        }
        if (!sameQuestion) {
            $scope.tempData.push({answer: answer});
        }
    };

    $scope.fillInView = function () {
        if ($scope.enquete.doc.isPublished === true) {
            $location.path("/enquete/" + $scope.enquete.doc._id + "/fill");
        } else {
            console.log("uuh dinges is niet gepublished");
        }
    };

    $scope.resultView = function () {
        if ($scope.enquete.doc.isPublished === true) {
            $location.path("/enquete/" + $scope.enquete.doc._id + "/result");
        } else {
            console.log("uuh dinges is niet gepublished");
        }
    };

    $scope.publishPage = function () {
        var tempBool, i;
        tempBool = true;
        for (i = 0; i < $scope.enquete.questions; i += 1) {
            if ($scope.enquete.questions[i].answer.length < 2) {
                tempBool = false;
            }
        }
        if (tempBool === false) {
            console.log("how about no");
        } else {
            $scope.enquete.doc.isPublished = true;
            $scope.save();
            $location.path("/sinterklaas");
        }
    };

    $scope.saveEnquete = function () {
        var i;
        for (i = 0; i < $scope.tempData.length; i += 1) {
            console.log("index", i);
            $scope.tempData[i].answer.chosenBy = {loggedIn_ID: window.crud.getCookieFromJar("loggedIn_ID")};
            $scope.postAnswer($scope.tempData[i]);
        }
        $location.path("/back");
    };

    // remove enquete
    $scope.remove = function (id) {
        console.log('entering remove');
        if (id === undefined) {
            id = $routeParams._id;
        }
        enqueteService.enquete.remove({_id: id});
        $location.path("/enquete/");
    };

    //TODO klikken op een nieuwe vraag toevoegen en dan meteen op save drukken van de enquete crashed de server :D
    $scope.saveQuestion = function (id) {
        window.crud.hideElement('qDetail');
        passingService.prepForBroadcast('saveQuestion', id);
    };

    $scope.removeQuestion = function (id) {
        var i, elementMouseIsOver = document.elementFromPoint(event.clientX, event.clientY);
        elementMouseIsOver.parentNode.parentNode.parentNode.style.display = "none";
        console.log(elementMouseIsOver.parentNode.parentNode.parentNode.parentNode);
        elementMouseIsOver.parentNode.parentNode.parentNode.parentNode.remove();
        passingService.prepForBroadcast('removeQuestion', id);
    };

    $scope.saveAnswer = function (id, qID) {
        var messageArray = [id, qID];
        window.crud.hideElement('aDetail', 'newAbtn', true);
        passingService.prepForBroadcast('saveAnswer', messageArray);
    };

    $scope.postAnswer = function (answer) {
        console.log("answer getting posted");
        passingService.prepForBroadcast('postAnswer', answer);
    };

    $scope.removeAnswer = function (id, qID) {
        var messageArray = [id, qID],
            elementMouseIsOver = document.elementFromPoint(event.clientX, event.clientY);
        elementMouseIsOver.parentNode.parentNode.style.display = "none";
        elementMouseIsOver.parentNode.parentNode.remove();
        passingService.prepForBroadcast('removeAnswer', messageArray);
    };

    // CREATE, UPDATE enquete
    $scope.save = function () {
        if ($scope.enquete.doc && $scope.enquete.doc._id !== undefined) {
            console.log('entering update');
            enqueteService.enquete.update({_id: $scope.enquete.doc._id}, $scope.enquete.doc, function (res) {
                $scope.saveMedia($scope.enquete.doc._id);
                $scope.saveQuestion();
                $scope.saveAnswer();
                console.log("res enquete", res);
            });
        } else {
            $scope.enquete.doc = {
                poll: $scope.enquete.doc.poll,
                intro: $scope.enquete.doc.intro,
                logo: ""
            };
            console.log('entering save', $scope.enquete.doc);
            enqueteService.enquete.save({}, $scope.enquete.doc, function (res) {
                console.log(res);
                $scope.saveMedia(res.doc._id);
                console.log("res enquete", res);
            });
        }
    };

    // UPDATE enquete image
    $scope.updateImage = function (imageId, enqueteId) {
        var enquete;
        enquete = $scope.enquete.doc;
        enquete.logo = imageId;
        console.log('entering image update enquete');
        enqueteService.enquete.update({_id: enqueteId}, enquete, function (res) {
            console.log("res enquete image", res);
            if (window.location.href.indexOf("newEnquete") > -1) {
                $location.path("/enquete/" + enqueteId);
            }
        });
    };

    // CREATE, UPDATE media
    $scope.saveMedia = function (ownerObjectId) {
        var mediaDoc, imgHash;
        imgHash = document.getElementsByClassName("enqueteMedia")[0].src;
        mediaDoc = {
            path: imgHash
        };
        console.log(ownerObjectId);
        if ($scope.enquete.doc.logo !== "") {
            console.log('entering media update', $scope.enquete.doc.logo);
            mediaService.media.update({_id: $scope.enquete.doc.logo}, mediaDoc, function (res) {
                console.log("res media", res);
                $scope.updateImage($scope.enquete.doc.logo, ownerObjectId);
            });
        } else {
            console.log('entering media save');
            mediaService.media.save({}, mediaDoc, function (res) {
                console.log("res media", res);
                $scope.updateImage(res.doc._id, ownerObjectId);
            });
        }
    };
}

function QuestionDetailCtrl($scope, $routeParams, questionService, passingService, $location, mediaService) {
    document.body.style.backgroundImage = "none";
    document.getElementsByTagName("footer")[0].style.display = "block";
    $scope.question = questionService.question.get({_id: $routeParams._id}, function () {
    });

    // remove question
    $scope.remove = function (id) {
        console.log('entering remove');
        var i, q, removeQuestion;
        removeQuestion = function (q) {
            questionService.question.remove({_id: q._id}, function (res) {
                console.log("res question", res);
            });
        };
        for (i = 0; i < $scope.enquete.questions.length; i += 1) {
            q = $scope.enquete.questions[i];
            if (q._id === id) {
                removeQuestion(q);
            }
        }
    };

    $scope.$on('saveQuestion', function () {
        $scope.save(passingService.id);
    });

    $scope.$on('removeQuestion', function () {
        $scope.remove(passingService.id);
    });

    // CREATE, UPDATE question
    $scope.save = function (id) {
        //UPDATE
        var i, q, newQdoc, updateQuestion, elementMouseIsOver;

        updateQuestion = function () {
            questionService.question.update({_id: q._id}, q, function (res) {
                console.log("res question", res);
            });
        };
        for (i = 0; i < $scope.enquete.questions.length; i += 1) {
            q = $scope.enquete.questions[i];
            if (q._id !== undefined) {
                $scope.saveMedia(q._id);
                console.log('entering question update');
                updateQuestion(q);
            }
        }
        //CREATE
        if ($scope.question.doc !== undefined && $scope.question.doc._id === undefined) {
            console.log('entering save');
            //            newQdoc = $scope.question.doc; //bevat alle questions
            elementMouseIsOver = document.elementFromPoint(event.clientX, event.clientY);
            newQdoc = {
                question: $scope.question.doc.question,
                sort: "test",//TODO MOET NOG EEN ECHTE SORT WORDEN
                enqueteID: ""
            };
            newQdoc.enqueteID = $scope.enquete.doc._id;
            questionService.question.save({}, newQdoc, function (res) {
                //TODO rebuild page instead of refresh
                var newNode = document.createElement("div");
                newNode.className = "questionBox";
                newNode.innerHTML = '<div class="row">' +
                    '<p class="col-md-12 questionHeader">Vraag {{enquete.questions.indexOf(question) + 1}}:</p>' +
                    '<div class="col-md-7">' +
                    '<textarea rows="5" cols="50" data-ng-model="question.question" required value=""></textarea>' +
                    '<button class="btn delete" role="button" ng-click="removeQuestion(question._id);">' +
                    '</button>' +
                    '</div>' +
                    '<div class="mediaContainer col-md-5"> ' +
                    '<div flow-init="{singleFile:true}" flow-file-added="!!{png:1,gif:1,jpg:1,jpeg:1}[$file.getExtension()]" class="ng-scope">' +
                    '<div class="preview">' +
                    '<div class="thumbnail ng-hide" ng-show="$flow.files.length">' +
                    '<img class="questionMedia" flow-img="$flow.files[0]" src="">' +
                    '</div>' +
                    '</div>' +
                    '<div class="controlPanel">' +
                    '<div class="btnContainer">' +
                    '<span class="upload btn" ng-show="!$flow.files.length" flow-btn="">Select image<input type="file" style="visibility: hidden; position: absolute;"></span>' +
                    '<span class="upload btn" ng-show="$flow.files.length" flow-btn="">Change<input type="file" style="visibility: hidden; position: absolute;"></span>' +
                    '<span class="upload btn btn-danger" ng-show="$flow.files.length" ng-click="$flow.cancel()">Remove</span> </div><p class="instructionText">' +
                    'Only PNG,GIF,JPG,JEPG files allowed.</p>' +
                    '</div>                                    ' +
                    '</div>                                </div>                           ' +
                    '</div>                            <div class="row answerContainer">                                ' +
                    '<div class="checkbox col-md-6" class="input-group" data-ng-repeat="answer in question.answers">                                    ' +
                    '<div class="answerBox">                                        ' +
                    '<input type="text" data-ng-model="answer.answer" required value="">                                            ' +
                    '<button class="btn delete" role="button" ng-click="removeAnswer(answer._id);">                                            ' +
                    '</button>                                           ' +
                    '</div>                                        ' +
                    '</div>                                    ' +
                    '</div>                                    ' +
                    '<div data-ng-controller="AnswerDetailCtrl" data-ng-include data-src="getPartial()" class="newAnswer"></div>';
                console.log(elementMouseIsOver.parentNode.parentNode.parentNode);
//                elementMouseIsOver.parentNode.parentNode.parentNode.childNodes[3].appendChild(newNode.childNodes[0]);
                console.log("res question", res);
            });
        }
    };

    // UPDATE question image
    $scope.updateImage = function (id, qId) {
        var i, q, updateImage;
        updateImage = function (q) {
            questionService.question.update({_id: q._id}, q, function (res) {
                console.log("res question image", res);
            });
        };
        for (i = 0; i < $scope.enquete.questions.length; i += 1) {
            q = $scope.enquete.questions[i];
            if (q._id === qId) {
                console.log(id, q);
                q.media = id;
                updateImage(q);
            }
        }
    };

    // CREATE, UPDATE media
    $scope.saveMedia = function (ownerObjectId) {
        var mediaDoc, imgHash, questionIndex, mediaUpdateFn, mediaSaveFn, newMedia = true;
        for (questionIndex = 0; questionIndex < $scope.enquete.questions.length; questionIndex += 1) {
            if (ownerObjectId === $scope.enquete.questions[questionIndex]._id) {
                imgHash = document.getElementsByClassName("questionMedia")[questionIndex].src;
                mediaDoc = {
                    path: imgHash
                };
            }
        }
        console.log(ownerObjectId);
        console.log($scope);
//        console.log($scope.media.doc);
        if ($scope.enquete.questions !== "") {
            console.log('entering media update check loop');
            mediaUpdateFn = function (index) {
                mediaService.media.update({_id: $scope.enquete.questions[index].media}, mediaDoc, function (res) {
                    $scope.updateImage($scope.enquete.questions[index].media, ownerObjectId);
                    console.log("res media", res);
                });
            };
            for (questionIndex = 0; questionIndex < $scope.enquete.questions.length; questionIndex += 1) {
                if (ownerObjectId === $scope.enquete.questions[questionIndex]._id && $scope.enquete.questions[questionIndex].media !== "" && $scope.enquete.questions[questionIndex].media !== undefined) {
                    console.log('entering media update');
                    mediaUpdateFn(questionIndex);
                    newMedia = false;
                }
            }
            if (newMedia && mediaDoc.path.indexOf("data:") > -1) {
                console.log('entering media save');
                mediaService.media.save({}, mediaDoc, function (res) {
                    $scope.updateImage(res.doc._id, ownerObjectId);
                    console.log("res media", res);
                });
            }
        }
    };

    var partial;
    /**
     * Initial file to get the partial
     * @returns {*}
     */
    $scope.getPartial = function () {
        partial = "/partials/question-detail.html";
        $scope.src = partial;
        return partial;
    };
}

function AnswerDetailCtrl($scope, $routeParams, answerService, passingService, $location) {
    document.body.style.backgroundImage = "none";
    document.getElementsByTagName("footer")[0].style.display = "block";
    $scope.answer = answerService.answer.get({_id: $routeParams._id}, function () {
//        console.log('$scope.answer ', $scope.answer);
    });

    // remove answer
    $scope.remove = function (id, qId) {
        console.log('entering remove');
        var i, q, j, a, removeAnswer;
        removeAnswer = function (a) {
            answerService.answer.remove({_id: a._id}, function (res) {
                console.log("res answer", res);
            });
        };
        for (i = 0; i < $scope.enquete.questions.length; i += 1) {
            q = $scope.enquete.questions[i];
            if (q.answers !== null) {
                for (j = 0; j < q.answers.length; j += 1) {
                    a = q.answers[j];
                    if (a._id === id) {
                        removeAnswer(a);
                    }
                }
            }
        }
    };

    $scope.$on('saveAnswer', function () {
        $scope.save();
    });

    $scope.$on('postAnswer', function () {
        console.log(passingService.message);
        $scope.save(undefined, undefined, passingService.message);
    });

    $scope.$on('removeAnswer', function () {
        var id = passingService.message[0], qId = passingService.message[1];
        $scope.remove(id, qId);
    });

    // CREATE, UPDATE answer
    $scope.save = function (id, qId, answer) {
        var i, q, j, a, newAdoc, updateAnswer, html, div, elementMouseIsOver;
        //UPDATE
        updateAnswer = function (a) {
            console.log('entering answer update');
            answerService.answer.update({_id: a._id}, a, function (res) {
                console.log(res);
            });
        };
        if (answer === undefined) {
            if ($scope.answer.doc !== undefined && $scope.answer.doc._id === undefined) {
                //CREATE
                console.log('entering save');
                if (qId === undefined) {
                    console.log("no question selected!");
                } else {
                    elementMouseIsOver = document.elementFromPoint(event.clientX, event.clientY);
                    newAdoc = {
                        answer: $scope.answer.doc.answer,
                        questionID: qId
                    };
                    answerService.answer.save({}, newAdoc, function (res) {
                        html = '<div class="checkbox col-md-6 ng-scope" data-ng-repeat="answer in question.answers">' +
                            '<div class="answerBox">' +
                            '<input type="text" data-ng-model="answer.answer" required="" value="' +
                            $scope.answer.doc.answer + '" class="ng-pristine ng-valid ng-valid-required">' +
                            '<button class="btn delete" role="button" ng-click="removeAnswer(' +
                            res.doc._id + ');" style="margin-top: 0px;">' + //TODO vind een manier om deze ng-click werkend te maken
                            '</button>' +
                            '</div>' +
                            '</div>';
                        div = document.createElement('div');
                        div.innerHTML = html;
                        elementMouseIsOver.parentNode.parentNode.parentNode.childNodes[3].appendChild(div.childNodes[0]);
                        console.log("res answer", res);
                    });
                }
            } else {
                for (i = 0; i < $scope.enquete.questions.length; i += 1) {
                    q = $scope.enquete.questions[i];
                    if (q.answers !== null) {
                        for (j = 0; j < q.answers.length; j += 1) {
                            a = q.answers[j];
                            a.chosenBy = undefined;
                            if (a._id !== undefined) {
                                updateAnswer(a);
                            }
                        }
                    }
                }
            }
        }
        if (answer !== undefined) {
            console.log("id", answer.answer._id);
            answerService.answer.update({_id: answer.answer._id}, answer.answer);
//            $location.path("/back");
        }
    };

    var partial;
    /**
     * Initial file to get the partial
     * @returns {*}
     */
    $scope.getPartial = function () {
        partial = "/partials/answer-detail.html";
        $scope.src = partial;
        return partial;
    };
}