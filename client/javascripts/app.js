/*global angular */
"use strict";

/**
 * @see http://docs.angularjs.org/guide/concepts
 */
angular.module('myApp', [ 'myApp.services', 'myApp.directives', 'flow'])
    .config(['$routeProvider', function ($routeProvider) {

        // Get all users
        $routeProvider.when('/user', {
            templateUrl: 'partials/user-list.html',
            controller: UserListCtrl
        });

        // Get 1 user
        $routeProvider.when('/user/:_id', {
            templateUrl: 'partials/user-detail.html',
            controller: UserDetailCtrl
        });

        // Get all groups
        $routeProvider.when('/group', {
            templateUrl: 'partials/group-list.html',
            controller: GroupListCtrl
        });

        // Get 1 group
        $routeProvider.when('/group/:_id', {
            templateUrl: 'partials/group-detail.html',
            controller: GroupDetailCtrl
        });

        // Get all enquetes
        $routeProvider.when('/enquete', {
            templateUrl: 'partials/enquete-list.html',
            controller: EnqueteListCtrl
        });

        // Get all enquetes
        $routeProvider.when('/enquete/:_id/overview', {
            templateUrl: 'partials/enquete-overview.html',
            controller: EnqueteDetailCtrl
        });

        // templates
        $routeProvider.when('/templates', {
            templateUrl: 'partials/templates.html',
            controller: TemplateCtrl
        });

        // Get 1 enquete
        $routeProvider.when('/enquete/:_id', {
            templateUrl: 'partials/enquete-detail.html',
            controller: EnqueteDetailCtrl
        });

        // Get populate form
        $routeProvider.when('/enquete/:_id/fill', {
            templateUrl: 'partials/enquete-fillIn.html',
            controller: EnqueteDetailCtrl
        });

        // Get result page
        $routeProvider.when('/enquete/:_id/result', {
            templateUrl: 'partials/enquete-result.html',
            controller: EnqueteDetailCtrl
        });

        // Get result page
        $routeProvider.when('/newEnquete', {
            templateUrl: 'partials/enquete-new.html',
            controller: EnqueteDetailCtrl
        });

        // home page
        $routeProvider.when('/', {
            templateUrl: 'partials/home.html',
            controller: HomeCtrl
        });

        // about us page
        $routeProvider.when('/overons', {
            templateUrl: 'partials/aboutus.html',
            controller: AboutUsCtrl
        });

        // about us page
        $routeProvider.when('/hoewerkthet', {
            templateUrl: 'partials/uitleg.html',
            controller: HoeWerktHetCtrl
        });

        // contact page
        $routeProvider.when('/contact', {
            templateUrl: 'partials/contact.html',
            controller: ContactCtrl
        });

        // contact page
        $routeProvider.when('/registreren', {
            templateUrl: 'partials/register.html',
            controller: UserDetailCtrl
        });

        // When no valid route is provided
        $routeProvider.otherwise({
            redirectTo: "/"
        });

        $routeProvider.when('/back', {
            templateUrl: 'partials/back.html',
            controller: UserDetailCtrl
        });

        $routeProvider.when('/sinterklaas', {
            templateUrl: 'partials/sinterklaas-invul.html',
            controller: UserDetailCtrl
        });

    }]);