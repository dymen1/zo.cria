/*global angular */
"use strict";

/* Directives */
angular.module('myApp.directives', [])
    .directive('passwordMatch', [function () {
        return {
            restrict: 'A',
            scope: true,
            require: 'ngModel',
            link: function (scope, elem, attrs, control) {
                var checker = function () {
                    //get the value of the first password
                    var n = false, e1 = scope.$eval(attrs.ngModel), e2 = scope.$eval(attrs.passwordMatch);
                    //get the value of the other password
                    document.getElementById("passwordMsg").style.display = "none";
                    if (e1 !== undefined && e2 !== undefined) {
                        if (e1.length > 0 && e1.length >= e2.length - 1) {
                            if (e1 !== e2) {
                                document.getElementById("passwordMsg").style.display = "block";
                            } else {
                                n = true;
                            }
                        }
                    }
                    //set the form control to valid if both
                    //passwords are the same, else invalid
                    control.$setValidity("unique", n);
                };
                //runs checker and has a empty callback function,
                //this callback only runs if the return value of checker has changed
                scope.$watch(checker, function () {
                });
            }
        };
    }])
    .directive('checkStrength', function () {
        return {
            replace: false,
            restrict: 'EACM',
            scope: true,
            link: function (scope, elem, attrs, control) {
                var strength = {
                    colors: ['#F00', '#F90', '#FF0', '#9F0', '#0F0'],
                    mesureStrength: function (p) {
                        var _force = 0,
                            _regex = /[$-/:-?{-~!"^_`\[\]]/g,
                            _lowerLetters = /[a-z]+/.test(p),
                            _upperLetters = /[A-Z]+/.test(p),
                            _numbers = /[0-9]+/.test(p),
                            _symbols = _regex.test(p),
                            _flags = [_lowerLetters,
                                _upperLetters, _numbers,
                                _symbols];
                        var _passedMatches = $.grep(_flags, function (el) {
                            return el === true;
                        }).length;

                        _force += 2 * p.length + ((p.length >= 10) ? 1 : 0);
                        _force += _passedMatches * 10;

                        // penality (short password)
                        _force = (p.length <= 6) ? Math.min(_force, 10) : _force;

                        // penality (poor variety of characters)
                        _force = (_passedMatches === 1) ? Math.min(_force, 10) : _force;
                        _force = (_passedMatches === 2) ? Math.min(_force, 20) : _force;
                        _force = (_passedMatches === 3) ? Math.min(_force, 40) : _force;

                        return _force;
                    },
                    getColor: function (s) {
                        var idx = 0;
                        if (s <= 10) {
                            idx = 0;
                        } else if (s <= 20) {
                            idx = 1;
                        } else if (s <= 30) {
                            idx = 2;
                        } else if (s <= 40) {
                            idx = 3;
                        } else {
                            idx = 4;
                        }
                        return { idx: idx, col: this.colors[idx] };
                    }
                };
                scope.$watch(attrs.checkStrength, function () {
                    if (scope.$eval(attrs.ngModel) === '' || scope.$eval(attrs.ngModel) === undefined) {
                        elem.css({ "display": "none"  });
                    } else {
                        var leftPos, topPos, pw2elem = document.getElementById("pw2"), i, c = strength.getColor(strength.mesureStrength(scope.$eval(attrs.ngModel)));
                        pw2elem = $(pw2elem).offset({relativeTo: "body"})[0];
                        leftPos = pw2elem.offsetLeft + pw2elem.offsetWidth + 5 + "px";
                        topPos = pw2elem.offsetTop + 5 + "px";
                        elem.css({
                            "display": "block",
                            "left": leftPos,
                            "top": topPos
                        });
                        elem.children('li').css({ "background": "#DDD" });
                        for (i = 0; i <= c.idx; i += 1) {
                            elem.children('li')[i].style.background = c.col;
                        }
//                            .splice(0, c.idx)
//                            .css({ "background": c.col });
                    }
                });
            },
            template: '<li class="point"></li>' +
                '<li class="point"></li>' +
                '<li class="point"></li>' +
                '<li class="point"></li>' +
                '<li class="point"></li>'
        };

    });