/*global angular */
"use strict";

angular.module('myApp.services', ['ngResource'])
    .factory('userService', ['$resource', '$http',
        function ($resource) {
            var actions = {
                    'get': {method: 'GET'},
                    'save': {method: 'POST'},
                    'update': {method: 'PUT'},
                    'query': {method: 'GET', isArray: true},
                    'remove': {method: 'DELETE'}
                },
                db = {};
            db.user = $resource('/user/:_id', {}, actions);
            return db;
        }])
    .factory('groupService', ['$resource', '$http',
        function ($resource) {
            var actions = {
                    'get': {method: 'GET'},
                    'save': {method: 'POST'},
                    'update': {method: 'PUT'},
                    'query': {method: 'GET', isArray: true},
                    'remove': {method: 'DELETE'}
                },
                db = {};
            db.group = $resource('/group/:_id', {}, actions);
            return db;
        }])
    .factory('enqueteService', ['$resource', '$http',
        function ($resource) {
            var actions = {
                    'get': {method: 'GET'},
                    'save': {method: 'POST'},
                    'update': {method: 'PUT'},
                    'query': {method: 'GET', isArray: true},
                    'remove': {method: 'DELETE'}
                },
                db = {};
            db.enquete = $resource('/enquete/:_id', {}, actions);
            return db;
        }])
    .factory('questionService', ['$resource', '$http',
        function ($resource) {
            var actions = {
                    'get': {method: 'GET'},
                    'save': {method: 'POST'},
                    'update': {method: 'PUT'},
                    'query': {method: 'GET', isArray: true},
                    'remove': {method: 'DELETE'}
                },
                db = {};
            db.question = $resource('/question/:_id', {}, actions);
            return db;
        }])
    .factory('answerService', ['$resource', '$http',
        function ($resource) {
            var actions = {
                    'get': {method: 'GET'},
                    'save': {method: 'POST'},
                    'update': {method: 'PUT'},
                    'query': {method: 'GET', isArray: true},
                    'remove': {method: 'DELETE'}
                },
                db = {};
            db.answer = $resource('/answer/:_id', {}, actions);
            return db;
        }])
    .factory('mediaService', ['$resource', '$http',
        function ($resource) {
            var actions = {
                    'get': {method: 'GET'},
                    'save': {method: 'POST'},
                    'update': {method: 'PUT'},
                    'query': {method: 'GET', isArray: true},
                    'remove': {method: 'DELETE'}
                },
                db = {};
            db.media = $resource('/media/:_id', {}, actions);
            return db;
        }])
    .factory('passingService', function ($rootScope) {
        var sharedService = {};

        sharedService.target = '';
        sharedService.message = '';
        sharedService.id = '';

        sharedService.prepForBroadcast = function (target, msg) {
            this.target = target;
            this.message = msg;
            this.id = msg;
            this.broadcastItem();
        };

        sharedService.broadcastItem = function () {
            $rootScope.$broadcast(this.target);
        };

        return sharedService;
    });



