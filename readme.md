Zo. cria!
===========

Edo Spijker
507901
Dylan Hesse
499549



Installatie
============

Zo moet de applicatie moeten worden geïnstalleerd:


1. MEAN-stack opzetten (server installatie)
    * http://mean.io/#!/docs

2. Zorg dat de Node server draait en er SSH toegang is

3. Installatie (applicatie installatie)
    * Open een SSH verbinding met de Node server
    * clone van git adres: git clone https://dymen1@bitbucket.org/dymen1/zo.cria.git
    * Gebruik het commando "npm install" in /zo.cria/server
    * Maak op de schijf waar mongo is geintstalleerd een data/db map op de juiste plek(bijvoorbeeld C:/data/db)
    * Start de database service door het commando “mongod” te gebruiken in de bin folder van de mongo installatie map

4. Data import:
    * Open een SSH verbinding met de Node server
    * Navigeer naar de bin folder van de mongo installatie map, plaats hier de dump folder die wordt meegeleverd bij de website
    * Gebruik het commando “mongorestore”, er staat nu een enquete in de database
    * Als er naar een andere database dan de default database moet wordeng geïmporteerd gebruik dan:
    * http://docs.mongodb.org/manual/reference/program/mongorestore/#cmdoption--db of een van de andere opties

5. Het starten van de applicatie
    * Open een SSH verbinding met de Node server
    * Start de applicatie door naar /zo.cria/server te navigeren en het commando: “node app” uit te voeren.

6. De applicatie draait nu  op localhost/13337




Configuratie
==============

1. Om het IP adres aan te passen moet server/config/config.js geopend worden.
    * onder "port" kan het poort nummer aangepast worden, van bijvoorbeeld 13337 naar 40404.
    * ook kan de database collectie naam worden verandert van groep6 naar iets anders.


API tests
============

https://www.getpostman.com/collections/88097424ce4ddb30305b