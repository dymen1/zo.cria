"use strict";

/**
 * Created by EDDY on today.
 */

var mongoose = require('mongoose'),
    Template = mongoose.model('Template');

/**
 * this function creates a template, you can use this function using the post method on a request to /template
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// CREATE
exports.create = function (req, res) {

    console.log('create Template');

    var doc = new Template(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
};
/**
 * this function retrieves a list, you can use this function using the get method on a request to /template
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// RETRIEVE
exports.list = function (req, res) {
    var conditions, fields, options, sort;

    console.log('get all Templates.');

    conditions = {};
    fields = {};
    sort = {'modificationDate': -1};

    Template
        .find(conditions, fields, options)
        .sort(sort)
        .exec(function (err, doc) {
            console.log("found");
            var retObj = {
                meta: {"action": "list", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
};

/**
 * this function retrieves a individual template, you can use this function using the get method on a request to /template/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
exports.detail = function (req, res) {
    var conditions, fields, options, sort;

    console.log('get all Templates');

    conditions = {_id: req.params._id};
    fields = {};
    sort = {'createdAt': -1};

    Template
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        });
};
/**
 * this function updates a individual template, you can use this function using the put method on a request to /template/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// UPDATE
exports.update = function (req, res) {
    var conditions, update, options, callback;

    conditions = {_id: req.params._id};
    update = {
        name: req.body.name || '',
        description: req.body.description || ''
    };
    options = {multi: false};
    callback = function (err, doc) {
        var retObj = {
            meta: {"action": "update", 'timestamp': new Date(), filename: __filename},
            doc: doc[0],
            err: err
        };
        return res.send(retObj);
    };

    Template.findOneAndUpdate(conditions, update, options, callback);
};


/**
 * this function deletes a individual template, you can use this function using the delete method on a request to /template/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// DELETE
exports.delete = function (req, res) {
    var conditions, update, options, callback;

    console.log('deleting Template...\n ', req.params._id);


    conditions = {_id: req.params._id};
    callback = function (err, doc) {
        var retObj = {
            meta: {"action": "delete", 'timestamp': new Date(), filename: __filename},
            doc: doc[0],
            err: err
        };
        return res.send(retObj);
    };

    Template.remove(conditions, callback);
};