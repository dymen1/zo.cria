"use strict";

/**
 * Created by EDDY on today.
 */

var mongoose = require('mongoose'),
    Media = mongoose.model('Media');


// CREATE
exports.create = function (req, res) {

    var doc = new Media(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
};
/**
 * this function retrieves a list, you can use this function using the get method on a request to /answer
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// RETRIEVE
exports.list = function (req, res) {
    var conditions, fields, options, sort;

    console.log('get all Media.');

    conditions = {};
    fields = {};
    sort = {'modificationDate': -1};

    Media
        .find(conditions, fields, options)
        .sort(sort)
        .exec(function (err, doc) {
            console.log("found");
            var retObj = {
                meta: {"action": "list", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
};

/**
 * this function retrieves a individual answer, you can use this function using the get method on a request to /answer/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
exports.detail = function (req, res) {
    var conditions, fields, options, sort;


    conditions = {_id: req.params._id};
    fields = {};
    sort = {'createdAt': -1};

    Media
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        });
};
/**
 * this function updates a individual answer, you can use this function using the put method on a request to /answer/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// UPDATE
exports.update = function (req, res) {
    var conditions, update, options, callback;

    conditions = {_id: req.params._id};
    update = {
        name: req.body.name || '',
        path: req.body.path || ''
    };
    options = {multi: false};
    callback = function (err, doc) {
        console.log("req", req);
        console.log("doc", doc);
        var retObj = {
            meta: {"action": "update", 'timestamp': new Date(), filename: __filename},
            doc: doc[0],
            err: err
        };
        return res.send(retObj);
    };

    Media.findOneAndUpdate(conditions, update, options, callback);
};

/**
 * this function deletes a individual enquete, you can use this function using the delete method on a request to /enquete/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// DELETE
exports.delete = function (req, res) {
    var conditions, update, options, callback;

    console.log('deleting Media...\n ', req.params._id);


    conditions = {_id: req.params._id};
    callback = function (err, doc) {
        var retObj = {
            meta: {"action": "delete", 'timestamp': new Date(), filename: __filename},
            doc: doc[0],
            err: err
        };
        return res.send(retObj);
    };

    Media.remove(conditions, callback);
};