"use strict";

/**
 * Created by EDDY on today.
 */

var mongoose = require('mongoose'),
    Group = mongoose.model('Group');

/**
 * this function creates a group, you can use this function using the post method on a request to /group
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// CREATE
exports.create = function (req, res) {

    console.log('create Group');

    var doc = new Group(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
};
/**
 * this function retrieves a list, you can use this function using the get method on a request to /group
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// RETRIEVE
exports.list = function (req, res) {
    var conditions, fields, options, sort;

    console.log('get all Groups.');

    conditions = {};
    fields = {};
    sort = {'modificationDate': -1};

    Group
        .find(conditions, fields, options)
        .sort(sort)
        .exec(function (err, doc) {
            console.log("found group");
            var retObj = {
                meta: {"action": "list", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
};

/**
 * this function retrieves a individual group, you can use this function using the get method on a request to /group/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
exports.detail = function (req, res) {
    var conditions, fields, options, sort;

    console.log('get all Groups');

    conditions = {_id: req.params._id};
    fields = {};
    sort = {'createdAt': -1};

    Group
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        });
};
/**
 * this function updates a individual group, you can use this function using the put method on a request to /group/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// UPDATE
exports.update = function (req, res) {
    var conditions, update, options, callback;

    conditions = {_id: req.params._id};
    update = {
        name: req.body.name || '',
        description: req.body.description || ''
    };
    options = {multi: false};
    callback = function (err, doc) {
        var retObj = {
            meta: {"action": "update", 'timestamp': new Date(), filename: __filename},
            doc: doc[0],
            err: err
        };
        return res.send(retObj);
    };

    Group.findOneAndUpdate(conditions, update, options, callback);
};

/**
 * this function deletes a individual group, you can use this function using the delete method on a request to /group/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// DELETE
exports.delete = function (req, res) {
    var conditions, update, options, callback;

    console.log('deleting Group...\n ', req.params._id);


    conditions = {_id: req.params._id};
    callback = function (err, doc) {
        var retObj = {
            meta: {"action": "delete", 'timestamp': new Date(), filename: __filename},
            doc: doc[0],
            err: err
        };
        return res.send(retObj);
    };

    Group.remove(conditions, callback);
};