"use strict";
/**
 * Created by EDDY on today.
 */

var mongoose = require('mongoose'),
    Answer = mongoose.model('Answer');


/**
 * this function creates a answer, you can use this function using the post method on a request to /answer
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// CREATE
exports.create = function (req, res) {

    console.log('create Answer');

    var doc = new Answer(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
};


/**
 * this function retrieves a list, you can use this function using the get method on a request to /answer
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// RETRIEVE
exports.list = function (req, res) {
    var conditions, fields, options, callback, sort;

    console.log('get all Answers.');

    conditions = {};
    fields = {};
    sort = {'modificationDate': -1};

    Answer
        .find(conditions, fields, options)
        .sort(sort)
        .exec(function (err, doc) {
            console.log("found");
            var retObj = {
                meta: {"action": "list", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
};


/**
 * this function retrieves a individual answer, you can use this function using the get method on a request to /answer/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
exports.detail = function (req, res) {
    var conditions, fields, options, callback, sort;

    console.log('get a specific answer(s)');

    conditions = {_id: req.params._id};
    fields = {};
    sort = {'createdAt': -1};

    Answer
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        });
};

/**
 * this function updates a individual answer, you can use this function using the put method on a request to /answer/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// UPDATE
exports.update = function (req, res) {
    var conditions, update, options, tempChosenBy, callback, sort;

    conditions = {_id: req.params._id};
    Answer
        .find(conditions, options)
        .sort(sort)
        .exec(function (err, doc) {
            console.log(req.body);
            console.log(doc);
            if (req.body.chosenBy !== undefined) {
                if (doc[0].chosenBy === undefined) {
                    tempChosenBy = req.body.chosenBy;
                    console.log("make chosenby");
                } else {
                    tempChosenBy = doc[0].chosenBy;
                    tempChosenBy.push(req.body.chosenBy);
                    console.log("add chosenby");
                }
            } else if (doc[0] !== undefined) {
                tempChosenBy = doc[0].chosenBy;
            } else {
                doc = req.body;
            }
            update = {
                chosenBy: tempChosenBy || '',
                answer: req.body.answer || '',
                questionID: req.body.questionID || '',
                media: req.body.media || ''
            };
            options = {multi: false};
            callback = function (err, doc) {
                var retObj = {
                    meta: {"action": "update", 'timestamp': new Date(), filename: __filename},
                    doc: doc[0],
                    err: err
                };
                return res.send(retObj);
            };


            Answer.findOneAndUpdate(conditions, update, options, callback);
        });
};


/**
 * this function deletes a individual answer, you can use this function using the delete method on a request to /answer/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// DELETE
exports.delete = function (req, res) {
    var conditions, update, options, callback;

    console.log('deleting Answer...\n ', req.params._id);


    conditions = {_id: req.params._id};
    callback = function (err, doc) {
        var retObj = {
            meta: {"action": "delete", 'timestamp': new Date(), filename: __filename},
            doc: doc[0],
            err: err
        };
        return res.send(retObj);
    };

    Answer.remove(conditions, callback);
};