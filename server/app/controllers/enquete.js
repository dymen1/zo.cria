"use strict";

/**
 * Created by EDDY on today.
 */

var mongoose = require('mongoose'),
    Enquete = mongoose.model('Enquete'),
    Question = mongoose.model('Question'),
    Answer = mongoose.model('Answer');


/**
 * this function creates a enquete, you can use this function using the post method on a request to /enquete
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// CREATE
exports.create = function (req, res) {

    console.log('create Enquete');

    var doc = new Enquete(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
};
/**
 * this function retrieves a list, you can use this function using the get method on a request to /enquete
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// RETRIEVE
exports.list = function (req, res) {
    var conditions = {},
        fields = {},
        options,
        sort = {'modificationDate': -1};

    console.log('get all Enquetes.');

    Enquete
        .find(conditions, fields, options)
        .sort(sort)
        .exec(function (err, doc) {
            console.log("found");
            var retObj = {
                meta: {"action": "list", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
};

/**
 * this function retrieves a individual enquete, you can use this function using the get method on a request to /enquete/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
exports.detail = function (req, res) {
    var conditions = {_id: req.params._id},
        fields = {},
        options,
        sort = {'createdAt': -1},
        retObj;

    Enquete
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            Question
                .find({enqueteID: req.params._id}, function (err, questionsDoc) {
                    //this makes sure that at enquete details will get send to the client if a enquete has no questions
                    if (typeof questionsDoc[0] !== "object") {
                        console.log("DONE");
                        retObj = {
                            meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                            doc: doc[0],
                            questions: questionsDoc,
                            err: err
                        };
                        return res.send(retObj);
                    }
                    var i, thirdReqCounter = 0;
                    for (i = 0; i < questionsDoc.length; i += 1) {
                        //TODO replace this code by adding answers to question see enquete model (tried this it broke it)
                        questionsDoc[i] = {
                            __v: questionsDoc[i].__v,
                            _id: questionsDoc[i]._id,
                            enqueteID: questionsDoc[i].enqueteID,
                            question: questionsDoc[i].question,
                            media: questionsDoc[i].media,
                            sort: questionsDoc[i].sort,
                            modificationDate: questionsDoc[i].modificationDate,
                            answers: null
                        };
                        Answer
                            .find({questionID: questionsDoc[i]._id}, function (err, answerDoc) {
                                thirdReqCounter += 1;
                                var j;
                                for (j = 0; j < questionsDoc.length; j += 1) {
                                    if (answerDoc[0] !== undefined) {
                                        if (String(questionsDoc[j]._id) === String(answerDoc[0].questionID)) {
                                            questionsDoc[j].answers = answerDoc;
                                        }
                                    }
                                }
                                if (i === thirdReqCounter) {
                                    console.log("DONE");
                                    retObj = {
                                        meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                                        doc: doc[0],
                                        questions: questionsDoc,
                                        err: err
                                    };
                                    return res.send(retObj);
                                }
                            });
                    }
                });
        });
};
/**
 * this function updates a individual enquete, you can use this function using the put method on a request to /enquete/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// UPDATE
exports.update = function (req, res) {
    var conditions = {_id: req.params._id},
        update = {
            poll: req.body.poll || '',
            isPublished: req.body.isPublished || '',
            intro: req.body.intro || '',
            logo: req.body.logo || ''
        },
        options = {multi: false},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "update", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        };

    Enquete.findOneAndUpdate(conditions, update, options, callback);
};


/**
 * this function deletes a individual enquete, you can use this function using the delete method on a request to /enquete/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// DELETE
exports.delete = function (req, res) {
    var conditions = {_id: req.params._id},
        update,
        options,
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "delete", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        };

    console.log('deleting Enquete...\n ', req.params._id);

    Enquete.remove(conditions, callback);
};