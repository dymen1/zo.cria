/**
 * Created by EDDY on today.
 */

"use strict";

var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    passwordHash = require('password-hash');

/**
 * this function creates a user, you can use this function using the post method on a request to /user
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// CREATE
exports.create = function (req, res) {

    console.log('create User');

    req.body.password = passwordHash.generate(req.body.password || "topSecret!");

    var doc = new User(req.body);

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
};
/**
 * this function retrieves a list, you can use this function using the get method on a request to /user
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// RETRIEVE
exports.list = function (req, res) {
    var conditions, fields, sort, options;

    console.log('get all Users.');

    conditions = {};
    fields = {};
    sort = {'modificationDate': -1};

    User
        .find(conditions, fields, options)
        .sort(sort)
        .exec(function (err, doc) {
            console.log("found  user");
            var retObj = {
                meta: {"action": "list", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
};

/**
 * this function retrieves a individual user, you can use this function using the get method on a request to /user/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
exports.detail = function (req, res) {
    var conditions, fields, sort, options;

    console.log('get 1 User');

    conditions = {_id: req.params._id};
    fields = {};
    sort = {'createdAt': -1};

    User
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        });
};
/**
 * this function updates a individual user, you can use this function using the put method on a request to /user/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// UPDATE
exports.update = function (req, res) {
    var conditions, fields, sort, update, options, callback;

    conditions = {_id: req.params._id};
    fields = {};
    sort = {'createdAt': -1};

    User
        .find(conditions, fields, options)
        .exec(function (err, doc) {

            console.log(doc);
            if (req.body.Password !== doc[0].password) {
                update = {
                    Password: req.body.Password || ''
                };
            }
        });
    conditions = {_id: req.params._id};
    update = {
        username: req.body.username || '',
        email: req.body.email || '',
        sex: req.body.sex || '',
        age: req.body.age || ''
    };
    options = {multi: false};
    callback = function (err, doc) {
        var retObj = {
            meta: {"action": "update", 'timestamp': new Date(), filename: __filename},
            doc: doc[0],
            err: err
        };
        return res.send(retObj);
    };

    User.findOneAndUpdate(conditions, update, options, callback);
};


/**
 * this function deletes a individual user, you can use this function using the delete method on a request to /user/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// DELETE
exports.delete = function (req, res) {
    var conditions, update, options, callback;

    console.log('deleting User...\n ', req.params._id);


    conditions = {_id: req.params._id};
    callback = function (err, doc) {
        var retObj = {
            meta: {"action": "delete", 'timestamp': new Date(), filename: __filename},
            doc: doc[0],
            err: err
        };
        return res.send(retObj);
    };

    User.remove(conditions, callback);
};