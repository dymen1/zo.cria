"use strict";

/**
 * Created by EDDY on today.
 */

var mongoose = require('mongoose'),
    Question = mongoose.model('Question');
/**
 * this function creates a question, you can use this function using the post method on a request to /question
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// CREATE
exports.create = function (req, res) {
    var conditions, fields, options, max, sort, i, doc;
    max = 0;
    console.log('create Question');


    conditions = {
        enqueteID: req.body.enqueteID
    };
    fields = {};
    sort = {'sortOrder': -1};
    Question
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            for (i = 0; i < doc.length; i += 1) {
                if (doc[i].sortOrder > max) {
                    max = doc[i].sortOrder;
                }
            }


        });


    doc = new Question(req.body);
    doc.sortOrder = max + 1;

    doc.save(function (err) {
        var retObj = {
            meta: {"action": "create", 'timestamp': new Date(), filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
};
/**
 * this function retrieves a list, you can use this function using the get method on a request to /question
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// RETRIEVE
exports.list = function (req, res) {
    var conditions, fields, options, sort;

    console.log('get all Questions.');

    conditions = {};
    fields = {};
    sort = {'createdAt': -1};

    Question
        .find(conditions, fields, options)
        .sort(sort)
        .exec(function (err, doc) {
            console.log("found");
            var retObj = {
                meta: {"action": "list", 'timestamp': new Date(), filename: __filename},
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
};

/**
 * this function retrieves a individual question, you can use this function using the get method on a request to /question/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
exports.detail = function (req, res) {
    var conditions, fields, options, sort;

    console.log('get all Questions');

    conditions = {_id: req.params._id};
    fields = {};
    sort = {'createdAt': -1};

    Question
        .find(conditions, fields, options)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "detail", 'timestamp': new Date(), filename: __filename},
                doc: doc[0],
                err: err
            };
            return res.send(retObj);
        });
};
/**
 * this function updates a individual question, you can use this function using the put method on a request to /question/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// UPDATE
exports.update = function (req, res) {
    var conditions, update, options, callback;

    conditions = {_id: req.params._id};
    update = {
        question: req.body.question || '',
        enqueteID: req.body.enqueteID || '',
        media: req.body.media || '',
        answers: req.body.answers,
        sort: req.body.sort || ''
    };
    options = {multi: false};
    callback = function (err, doc) {
        var retObj = {
            meta: {"action": "update", 'timestamp': new Date(), filename: __filename},
            doc: doc[0],
            err: err
        };
        return res.send(retObj);
    };

    Question.findOneAndUpdate(conditions, update, options, callback);
};

/**
 * this function deletes a individual question, you can use this function using the delete method on a request to /question/_id
 *
 * @param req is the doc that is send from the client to the server
 * @param res is the response from the server to the client
 */
// DELETE
exports.delete = function (req, res) {
    var conditions, update, options, callback;
    console.log('deleting Question...\n ', req.params._id);


    conditions = {_id: req.params._id};
    callback = function (err, doc) {
        var retObj = {
            meta: {"action": "delete", 'timestamp': new Date(), filename: __filename},
            doc: doc[0],
            err: err
        };
        return res.send(retObj);
    };

    Question.remove(conditions, callback);
}