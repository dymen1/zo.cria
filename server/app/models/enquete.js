/**
 * Module dependencies.
 */
var mongoose, Schema;
mongoose = require('mongoose');
Schema = mongoose.Schema;


var questionSchema = new Schema({
    _id: {type: Schema.Types.ObjectId, ref: "Question"}
});
/**
 * schema definition
 */
var schemaName = new Schema({
    poll: {type: String, required: true},
    intro: {type: String, required: false},
    logo: {type: String, required: false},
    isPublished: {type: Boolean, required: true, "default": false},
    questions: [questionSchema], // <----- sub document
    modificationDate: {type: Date, "default": Date.now}
});


var modelName = "Enquete";
var collectionName = "enquetes"; // Naming convention is plural.
mongoose.model(modelName, schemaName, collectionName);

