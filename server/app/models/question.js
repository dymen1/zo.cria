/**
 * Module dependencies.
 */
var mongoose, Schema;
mongoose = require('mongoose');
Schema = mongoose.Schema;


var answerSchema = new Schema({
    _id: {type: Schema.Types.ObjectId, ref: "Answer"}
});
/**
 * schema definition
 */
var schemaName = new Schema({
    question: {type: String, required: true},
    sort: {type: String, required: true},
//    sortOrder:{type: Number, required: true},
    enqueteID: {type: String, required: true},
    answers: [answerSchema], // <----- sub document
    media: {type: Object},
    modificationDate: {type: Date, "default": Date.now},
    meta: {} // anything goes
});


var modelName = "Question";
var collectionName = "questions"; // Naming convention is plural.
mongoose.model(modelName, schemaName, collectionName);

