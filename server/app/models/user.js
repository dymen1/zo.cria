/**
* Module dependencies.
*/
var mongoose, Schema;
mongoose = require('mongoose');
Schema = mongoose.Schema;

/**
* schema definition
*/
var schemaName = new Schema({
    firstname: {type: String},
    surname: {type: String},
    username: {type: String, required: true},
//    userID: {type: String, required: true},
//    sex: {type: String},
//    age: {type: String},
//    address: {type: String},
    email: {type: String, required: true},
    password: {type: String, required: true},
    modificationDate: {type: Date, "default": Date.now},
    meta: {} // anything goes
});


var modelName = "User";
var collectionName = "users"; // Naming convention is plural.
mongoose.model(modelName, schemaName, collectionName);

