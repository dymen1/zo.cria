/**
 * Module dependencies.
 */
var mongoose, Schema;
mongoose = require('mongoose');
Schema = mongoose.Schema;


/**
 * schema definition
 */
var schemaName = new Schema({
    name: {type: String, required: false},
    path: {type: String, required: true},
    meta: {}, // anything goes
    modificationDate: {type: Date, "default": Date.now}
});


var modelName = "Media";
var collectionName = "medias"; // Naming convention is plural.
mongoose.model(modelName, schemaName, collectionName);

