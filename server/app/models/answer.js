"use strict";

/**
 * Module dependencies.
 */
var mongoose, Schema;
mongoose = require('mongoose');
Schema = mongoose.Schema;

/**
 * schema definition
 */
var schemaName = new Schema({
    chosenBy: {type: Array, required: false},
    answer: {type: String, required: true},
    questionID: {type: String, required: true},
    media: {type: String},
    modificationDate: {type: Date, "default": Date.now}
});


var modelName = "Answer";
var collectionName = "answers"; // Naming convention is plural.
mongoose.model(modelName, schemaName, collectionName);

