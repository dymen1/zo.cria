"use strict";
module.exports = function (app) {
    /*  request routes
     ---------------
     We create a variable "user" that holds the controllers object.
     We map the URL to a method in the created variable "user".
     In this example is a mapping for every CRUD action.
     */
    var controller = require('../app/controllers/user.js');

    function ensureAuthenticated(req, res, next) {
        if (req.isAuthenticated()) {
            return next();
        }

        var retObj = {
            isVerified: false,
            meta: {
                description: "You are not logged in.",
                function: "ensureAuthenticated (local)",
                timestamp: new Date(),
                filename: __filename
            }
        };
        return res.send(retObj);
    }

    function ensureNotAuthenticated(req, res, next) {
        if (req.isAuthenticated()) {
            var retObj = {
                isVerified: false,
                meta: {
                    description: "You are allready logged in.",
                    function: "ensureAuthenticated (local)",
                    timestamp: new Date(),
                    filename: __filename
                }
            };
            return res.send(retObj);
        }
        return next();
    }

    // CREATE
    app.post('/user', ensureNotAuthenticated, controller.create);
    // RETRIEVE
    app.get('/user', ensureAuthenticated, controller.list);
    app.get('/user/:_id', controller.detail);

    // UPDATE
    app.put('/user/:_id', ensureAuthenticated, controller.update);

    // DELETE
    app.delete('/user/:_id', ensureAuthenticated, controller.delete);

}
