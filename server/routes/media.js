"use strict";
module.exports = function (app) {
    /*  media routes
     ---------------
     We create a variable "user" that holds the controllers object.
     We map the URL to a method in the created variable "user".
     In this example is a mapping for every CRUD action.
     */
    var controller = require('../app/controllers/media.js');
   // var security = require('../config/security.js');

    function ensureAuthenticated(req, res, next) {
        if (req.isAuthenticated()) {
            return next();
        }

        var retObj = {
            isVerified: false,
            meta: {
                description: "You are not logged in.",
                function: "ensureAuthenticated (local)",
                timestamp: new Date(),
                filename: __filename
            }
        };
        return res.send(retObj);
    }

    // CREATE
    app.post('/media', ensureAuthenticated, controller.create);
   // RETRIEVE
    app.get('/media', ensureAuthenticated, controller.list);
    app.get('/media/:_id', ensureAuthenticated, controller.detail);

    // UPDATE
    app.put('/media/:_id', ensureAuthenticated, controller.update);

    // DELETE
    app.delete('/media/:_id', ensureAuthenticated, controller.delete);

}
