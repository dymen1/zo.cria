// creator Edo Spijker

module.exports = {
    development: {
        debugging: false,
        db: 'mongodb://localhost:27017/groep6', // change p123456 with the name of your database (p + student number)
        port: 13337,                             // change 3000 with your port number//test
        AccessControlAllowMethods: "GET,PUT,POST,DELETE",
        allowedDomains: "*"
    },
    test: {
    },
    production: {
    }
};