/**
 * Created by theotheu on 27-10-13.
 */

// Export module
module.exports = function (config) {
    var nodemailer = require("nodemailer");

    // create reusable transport method (opens pool of SMTP connections)
    var smtpTransport = nodemailer.createTransport("SMTP");

    var htmlStr = "Hello world ∑ A-Ω started on port " + config.port + " from path " + __dirname;
    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: "CRIATester ∑ A-Ω  <theo.theunissen@inter.nl.net>", // sender address
        to: config.mailTo, // list of receivers
        subject: "Hello, the server/app.js successfully (re)started ✔ " + (new Date()), // Subject line
        text: htmlStr, // plaintext body
        html: "<b>" + htmlStr + "</b>" // html body
    };
    // send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
            console.log(error);
        } else {
            console.log("Message sent: " + response.message);
        }

        // if you don't want to use this transport object anymore, uncomment following line
        smtpTransport.close(); // shut down the connection pool, no more messages
    })
};